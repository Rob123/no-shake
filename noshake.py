import memory

from memory import DataType
from memory import Convention

from memory.hooks import PreHook

#void UTIL_ScreenShake( const Vector &center, float amplitude, float frequency, float duration, float radius, ShakeCommand_t eCommand, bool bAirShake )
server = memory.find_binary('tf/bin/server')

identifier = '_Z16UTIL_ScreenShakeRK6Vectorffff14ShakeCommand_tb'
a = server[identifier].make_function(
    Convention.THISCALL,
    (DataType.POINTER, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.POINTER, DataType.BOOL),
    DataType.VOID)

@PreHook(a)
def _Z16UTIL_ScreenShakeRK6Vectorffff14ShakeCommand_tb(args):
    return False
